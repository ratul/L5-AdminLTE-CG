# InfancyIT CRUD Genertor module Gen 2
### Boilerplate of Laravel with InfyOm Laravel Generator for AdminLTE Templates

## TO DO:
1. cd into laradock
2. edit docker-compose.yml to change settings of docker container as needed (default is good for this repo)
3. docker-compose build
4. docker-compose up -d nginx mysql phpmyadmin (add other containers based on need)
5. docker-compose ps (check if the docker containers are running correctly)
6. browse to localhost to get started

## To Migrate: (if needed)
1. cd into laradock
2. docker-compose exec workspace bash (enter main docker workspace bash)

from here you can run all the php artisan and composer commands. So do migrate and update composer if needed.

## NOTE: The composer inside the docker container is not the global host instance of composer. so it might not be updated depending the time you clone this repo. So run composer selfupdate from docker workspace bash if needed. Enjoy!

## USE:
http://labs.infyom.com/laravelgenerator/docs/5.3/getting-started (for generating scaffold and apis)
http://labs.infyom.com/laravelgenerator/docs/5.3/gui-interface (for gui-interface-builder)


# Cheerio!!
